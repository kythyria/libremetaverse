﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenMetaverse.Http;

namespace OpenMetaverse.TestClient
{
    class MaterialInfoCommand : Command
    {
        const string UsageText = "Usage: materialinfo <uuid> ...";

        public MaterialInfoCommand(TestClient testClient)
        {
            Name = "materialinfo";
            Description = "Requests material data. " + UsageText;
            Category = CommandCategory.Other;
        }

        public override string Execute(string[] args, UUID fromAgentID)
        {
            if (args.Length == 0)
            {
                return UsageText;
            }

            Uri uri = Client.Network.CurrentSim.Caps.CapabilityURI("RenderMaterials");
            var cc = new CapsClient(uri);

            var msg = new Messages.Linden.RenderMaterialsRequest();

            var ids = new List<UUID>();
            foreach(var i in args)
            {
                UUID matid;
                if(!UUID.TryParse(i, out matid))
                {
                    return "Malformed UUID";
                }
                ids.Add(matid);
            }
            msg.MaterialIds = ids.ToArray();

            var resp = cc.GetResponse(msg.Serialize(), StructuredData.OSDFormat.Xml, Client.Settings.CAPS_TIMEOUT);

            var responsemessage = new Messages.Linden.RenderMaterialsMessage();
            responsemessage.Deserialize((StructuredData.OSDMap)resp);
            var resulttext = new StringBuilder();

            foreach(var i in responsemessage.Definitions)
            {
                resulttext.AppendFormat("{0}: {1}{2}", i.MaterialID, i.Material);
                resulttext.AppendLine();
            }
            return resulttext.ToString();
        }
    }
}
