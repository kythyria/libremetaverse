﻿using OpenMetaverse;
using OpenMetaverse.StructuredData;
using System;

namespace OpenMetaverse
{
    public enum MaterialAlphaMode : byte
    {
        None = 0,
        Blend = 1,
        Mask = 2,
        Emissive = 3,
        Default = 4
    }

    public class AdvancedMaterial : ICloneable
    {
        private const float Multiplier = 10000.0f;
        private static OSDInteger MultiplyForOSD(float f) => new OSDInteger((int)Math.Floor(f + 0.5f));
        private static float DivideFromOSD(OSD i) => i.AsInteger() / Multiplier;
        private static Color4 ColorFromOSD(OSDArray a) => new Color4(
            (byte)a[0].AsInteger(),
            (byte)a[1].AsInteger(),
            (byte)a[2].AsInteger(),
            (byte)a[3].AsInteger()
        );
        private static OSDArray OSDFromColor(Color4 c)
        {
            var b = c.GetBytes();
            var a = new OSDArray();
            a.Add(OSD.FromInteger(b[0]));
            a.Add(OSD.FromInteger(b[1]));
            a.Add(OSD.FromInteger(b[2]));
            a.Add(OSD.FromInteger(b[3]));
            return a;
        }

        public UUID NormalMapId { get; set; }
        public float NormalOffsetX { get; set; }
        public float NormalOffsetY { get; set; }
        public float NormalRepeatX { get; set; }
        public float NormalRepeatY { get; set; }
        public float NormalRotation { get; set; }

        public UUID SpecularMapId { get; set; }
        public float SpecularRepeatX { get; set; }
        public float SpecularRepeatY { get; set; }
        public float SpecularOffsetX { get; set; }
        public float SpecularOffsetY { get; set; }
        public float SpecularRotation { get; set; }

        public Color4 SpecularColor { get; set; }
        public byte SpecularExponent { get; set; }
        public byte EnvironmentIntensity { get; set; }
        public byte AlphaMaskCutoff { get; set; }
        public MaterialAlphaMode AlphaMode { get; set; }

        public AdvancedMaterial()
        {

        }

        public AdvancedMaterial(AdvancedMaterial source)
        {
            NormalMapId          = source.NormalMapId;
            NormalOffsetX        = source.NormalOffsetX;
            NormalOffsetY        = source.NormalOffsetY;
            NormalRepeatX        = source.NormalRepeatX;
            NormalRepeatY        = source.NormalRepeatY;
            NormalRotation       = source.NormalRotation;

            SpecularMapId        = source.SpecularMapId;
            SpecularRepeatX      = source.SpecularRepeatX;
            SpecularRepeatY      = source.SpecularRepeatY;
            SpecularOffsetX      = source.SpecularOffsetX;
            SpecularOffsetY      = source.SpecularOffsetY;
            SpecularRotation     = source.SpecularRotation;

            SpecularColor        = source.SpecularColor;
            SpecularExponent     = source.SpecularExponent;
            EnvironmentIntensity = source.EnvironmentIntensity;
            AlphaMode            = source.AlphaMode;
        }

        public object Clone()
        {
            return new AdvancedMaterial(this);
        }

        public override int GetHashCode()
        {
            return NormalMapId.GetHashCode() ^
                NormalOffsetX.GetHashCode() ^
                NormalOffsetY.GetHashCode() ^
                NormalRepeatX.GetHashCode() ^
                NormalRepeatY.GetHashCode() ^
                NormalRotation.GetHashCode() ^
                SpecularMapId.GetHashCode() ^
                SpecularRepeatX.GetHashCode() ^
                SpecularRepeatY.GetHashCode() ^
                SpecularOffsetX.GetHashCode() ^
                SpecularOffsetY.GetHashCode() ^
                SpecularRotation.GetHashCode() ^
                SpecularColor.GetHashCode() ^
                SpecularExponent.GetHashCode() ^
                EnvironmentIntensity.GetHashCode() ^
                AlphaMode.GetHashCode();
        }

        public override string ToString()
        {
            return $"{{SpecularColor: {SpecularColor}, SpecularExponent: {SpecularExponent}, " +
                $"EnvironmentIntensity: {EnvironmentIntensity}, AlphaMaskCutoff: {AlphaMaskCutoff}," +
                $"AlphaMode: {AlphaMode}, NormalMapId: {NormalMapId}, NormalOffsetX: {NormalOffsetX}, " +
                $"NormalOffsetY: {NormalOffsetY}, NormalRepeatX: {NormalRepeatX}, NormalRepeatY: {NormalRepeatY}, " +
                $"NormalRotation: {NormalRotation}, SpecularMapId: {SpecularMapId}, " +
                $"SpecularOffsetX: {SpecularOffsetX}, SpecularOffsetY: {SpecularOffsetY}, " +
                $"SpecularRotation: {SpecularRotation}}}";
        }

        public OSDMap GetOSD()
        {
            return new OSDMap
            {
                ["AlphaMaskCutoff"] = OSD.FromInteger(AlphaMaskCutoff),
                ["DiffuseAlphaMode"] = OSD.FromInteger((int)AlphaMode),
                ["EnvIntensity"] = OSD.FromInteger(EnvironmentIntensity),
                ["NormMap"] = OSD.FromUUID(NormalMapId),
                ["NormOffsetX"] = MultiplyForOSD(NormalOffsetX),
                ["NormOffsetY"] = MultiplyForOSD(NormalOffsetY),
                ["NormRepeatX"] = MultiplyForOSD(NormalRepeatX),
                ["NormRepeatY"] = MultiplyForOSD(NormalRepeatY),
                ["NormRotation"] = MultiplyForOSD(NormalRotation),
                ["SpecColor"] = OSDFromColor(SpecularColor),
                ["SpecExp"] = OSD.FromInteger(SpecularExponent),
                ["SpecMap"] = OSD.FromUUID(SpecularMapId),
                ["SpecOffsetX"] = MultiplyForOSD(SpecularOffsetX),
                ["SpecOffsetY"] = MultiplyForOSD(SpecularOffsetY),
                ["SpecRepeatX"] = MultiplyForOSD(SpecularRepeatX),
                ["SpecRepeatY"] = MultiplyForOSD(SpecularRepeatY),
                ["SpecRotation"] = MultiplyForOSD(SpecularRotation)
            };
        }

        public static AdvancedMaterial FromOSD(OSDMap data)
        {
            OSDMap map = data as OSDMap;
            var ret = new AdvancedMaterial();
            ret.AlphaMaskCutoff = (byte)map["AlphaMaskCutoff"].AsInteger();
            ret.AlphaMode = (MaterialAlphaMode)map["DiffuseAlphaMode"].AsInteger();
            ret.EnvironmentIntensity = (byte)map["EnvIntensity"].AsInteger();
            ret.NormalMapId = map["NormMap"].AsUUID();
            ret.NormalOffsetX = DivideFromOSD(map["NormOffsetX"]);
            ret.NormalOffsetY = DivideFromOSD(map["NormOffsetY"]);
            ret.NormalRepeatX = DivideFromOSD(map["NormRepeatX"]);
            ret.NormalRepeatY = DivideFromOSD(map["NormRepeatY"]);
            ret.NormalRotation = DivideFromOSD(map["NormRotation"]);
            ret.SpecularColor = ColorFromOSD(map["SpecColor"] as OSDArray);
            ret.SpecularExponent = (byte)map["SpecExp"].AsInteger();
            ret.SpecularMapId = map["SpecMap"].AsUUID();
            ret.SpecularOffsetX = DivideFromOSD(map["SpecOffsetX"]);
            ret.SpecularOffsetY = DivideFromOSD(map["SpecOffsetY"]);
            ret.SpecularRepeatX = DivideFromOSD(map["SpecRepeatX"]);
            ret.SpecularRepeatY = DivideFromOSD(map["SpecRepeatY"]);
            ret.SpecularRotation = DivideFromOSD(map["SpecRotation"]);
            return ret;
        }
    }
}
